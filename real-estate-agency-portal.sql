-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2021 at 06:41 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `real-estate-agency-portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `featured` double DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `type`, `p_id`, `featured`, `user_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Apartment', 'apartment', 'category', NULL, 1, 1, 1, NULL, '2020-12-31 03:07:41', '2020-12-31 03:10:27'),
(2, 'Villa', 'villa', 'category', NULL, 1, 1, 1, '2021-06-03 15:17:18', '2020-12-31 03:08:00', '2020-12-31 03:10:18'),
(3, 'Houses', 'houses', 'category', NULL, 1, 1, 1, NULL, '2020-12-31 03:08:14', '2021-06-03 10:14:43'),
(4, 'Condo', 'condo', 'category', NULL, 1, 1, 1, '2021-06-03 15:17:29', '2020-12-31 03:08:30', '2020-12-31 03:09:58'),
(5, 'Land', 'land', 'category', NULL, 1, 1, 1, NULL, '2020-12-31 03:08:49', '2020-12-31 03:09:49'),
(6, 'New Developments', 'new-developments', 'category', NULL, 1, 1, 1, NULL, '2020-12-31 03:09:13', '2021-06-03 10:15:41'),
(14, 'CFA franc', 'CFA', 'currency', NULL, 1, 1, 1, NULL, '2020-12-31 03:16:50', '2020-12-31 03:16:50'),
(15, 'Number of Blocks', 'number', 'option', NULL, 1, 1, 0, NULL, '2020-12-31 03:17:53', '2021-01-10 23:44:10'),
(16, 'Bedrooms', 'number', 'option', NULL, 1, 1, 0, NULL, '2020-12-31 03:19:37', '2020-12-31 03:19:37'),
(17, 'Bathrooms', 'number', 'option', NULL, 1, 1, 0, NULL, '2020-12-31 03:20:11', '2020-12-31 03:20:11'),
(18, 'Floors', 'number', 'option', NULL, 1, 1, 1, NULL, '2020-12-31 03:21:03', '2021-01-10 13:23:13'),
(19, 'Bamenda', 'bamenda', 'states', NULL, 1, 1, 1, NULL, '2020-12-31 03:24:25', '2021-06-03 10:50:20'),
(20, 'Limbe', 'limbe', 'states', NULL, 1, 1, 1, NULL, '2020-12-31 03:25:23', '2021-06-03 10:57:17'),
(21, 'Dhaka', 'dhaka', 'cities', 19, 0, 1, 1, NULL, '2020-12-31 03:27:36', '2020-12-31 03:27:36'),
(22, 'Faridpur', 'faridpur', 'cities', 19, 0, 1, 1, NULL, '2020-12-31 03:28:31', '2020-12-31 03:28:31'),
(23, 'Chattogram', 'chattogram', 'cities', 20, 0, 1, 1, NULL, '2020-12-31 03:29:17', '2020-12-31 03:29:17'),
(24, 'Cox\'s Bazar', 'coxs-bazar', 'cities', 20, 1, 1, 1, NULL, '2020-12-31 03:30:51', '2020-12-31 03:30:51'),
(26, 'Sale', 'sale', 'status', NULL, 1, 1, 1, NULL, '2021-01-10 03:46:34', '2021-01-10 03:46:34'),
(27, 'Rent', 'rent', 'status', NULL, 1, 1, 1, '2021-06-06 14:48:11', '2021-01-10 03:46:39', '2021-01-10 03:46:39'),
(29, 'Projects', 'projects', 'status', NULL, 1, 1, 1, NULL, '2021-01-10 08:26:59', '2021-01-10 08:26:59'),
(30, 'Share', 'share', 'status', NULL, 1, 1, 1, '2021-06-06 14:48:18', '2021-01-10 08:28:37', '2021-01-10 08:28:37'),
(31, 'Buea', 'buea', 'states', NULL, 1, 1, 1, NULL, '2021-01-10 09:56:28', '2021-06-03 10:57:34'),
(32, 'Yaoundé', 'yaoundé', 'states', NULL, 1, 1, 1, NULL, '2021-01-10 09:57:58', '2021-06-03 10:57:48'),
(33, 'Douala', 'douala', 'states', NULL, 1, 1, 1, NULL, '2021-01-10 09:59:05', '2021-06-03 10:58:12'),
(34, 'Garoua', 'garoua', 'states', NULL, 1, 1, 1, NULL, '2021-01-10 10:02:50', '2021-06-03 10:58:29'),
(35, 'Wifi', 'wifi', 'feature', NULL, 1, 1, 1, NULL, '2021-01-10 12:53:51', '2021-01-10 12:53:51'),
(36, 'Swimming pool', 'swimming-pool', 'feature', NULL, 0, 1, 1, NULL, '2021-01-10 12:54:12', '2021-01-10 12:54:12'),
(37, 'Parking', 'parking', 'feature', NULL, 0, 1, 1, NULL, '2021-01-10 12:54:37', '2021-01-10 12:54:37'),
(38, 'Security', 'security', 'feature', NULL, 0, 1, 1, NULL, '2021-01-10 12:54:57', '2021-01-10 12:54:57'),
(39, 'Fitness center', 'fitness-center', 'feature', NULL, 0, 1, 1, NULL, '2021-01-10 12:55:39', '2021-01-10 12:55:39'),
(40, 'Balcony', 'balcony', 'feature', NULL, 0, 1, 1, NULL, '2021-01-10 12:56:00', '2021-01-10 12:56:00'),
(41, 'Hospital', 'hospital', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:01:58', '2021-01-10 13:01:58'),
(42, 'Super Market', 'super-market', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:02:45', '2021-01-10 13:02:45'),
(43, 'School', 'school', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:03:04', '2021-01-10 13:03:04'),
(44, 'Entertainment', 'entertainment', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:03:20', '2021-01-10 13:03:20'),
(45, 'Pharmacy', 'pharmacy', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:04:00', '2021-01-10 13:04:00'),
(46, 'Airport', 'airport', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:04:23', '2021-01-10 13:04:23'),
(47, 'Railways', 'railways', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:04:35', '2021-01-10 13:04:35'),
(48, 'Bus Stop', 'bus-stop', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:04:50', '2021-01-10 13:04:50'),
(49, 'Beach', 'beach', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:05:04', '2021-01-10 13:05:04'),
(50, 'Mall', 'mall', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:05:24', '2021-01-10 13:05:24'),
(51, 'Bank', 'bank', 'facilities', NULL, 0, 1, 1, NULL, '2021-01-10 13:05:40', '2021-01-10 13:05:40'),
(52, 'AMCoders', 'amcodersrzBcY', 'agency', NULL, NULL, 1, 1, NULL, '2021-01-11 09:53:20', '2021-01-11 10:11:04'),
(53, 'Fast Box', 'fast-box', 'agency', NULL, NULL, 1, 1, NULL, '2021-01-11 10:23:26', '2021-01-11 10:28:08'),
(54, 'Company Slogan', 'company-slogan', 'agency', NULL, NULL, 1, 1, NULL, '2021-01-11 10:31:19', '2021-01-11 10:31:19'),
(61, 'jomidar', 'en', 'lang', NULL, NULL, 1, 1, NULL, '2021-01-18 04:22:05', '2021-01-18 04:22:32'),
(62, 'jomidar', 'ar', 'lang', NULL, NULL, 1, 1, '2021-06-03 11:49:03', '2021-01-18 04:22:14', '2021-06-03 11:49:03'),
(63, 'BDT', 'TK', 'currency', NULL, 84, 1, 1, '2021-06-07 15:18:51', '2020-12-31 03:16:50', '2020-12-31 03:16:50'),
(64, 'Paypal', 'uploads/paypal.png', 'getway', NULL, 0, 1, 1, NULL, '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(65, 'stripe', 'uploads/stripe.png', 'getway', NULL, 0, 1, 1, NULL, '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(66, 'toyyibpay', 'uploads/toyyibpay.png', 'getway', NULL, 0, 1, 1, NULL, '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(67, 'Razorpay', 'uploads/razorpay.png', 'getway', NULL, 0, 1, 1, NULL, '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(68, 'Instamojo', 'uploads/instamojo.png', 'getway', NULL, 0, 1, 1, NULL, '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(69, 'Mollie', 'uploads/mollie.png', 'getway', NULL, 0, 1, 1, NULL, '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(70, 'Maroua', 'maroua', 'states', NULL, 1, 1, 1, NULL, '2021-06-03 10:58:56', '2021-06-03 10:58:56'),
(71, 'Baffoussam', 'baffoussam', 'states', NULL, 1, 1, 1, NULL, '2021-06-03 10:59:54', '2021-06-03 10:59:54'),
(72, 'Ngaoundere', 'ngaoundere', 'states', NULL, 1, 1, 1, NULL, '2021-06-03 11:00:12', '2021-06-03 11:00:12'),
(73, 'Bertoua', 'bertoua', 'states', NULL, 1, 1, 1, NULL, '2021-06-03 11:00:23', '2021-06-03 11:00:23'),
(74, 'Edea', 'edea', 'states', NULL, 1, 1, 1, NULL, '2021-06-03 11:00:39', '2021-06-03 11:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `categorymetas`
--

CREATE TABLE `categorymetas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorymetas`
--

INSERT INTO `categorymetas` (`id`, `category_id`, `type`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'credit_charge', '100', '2020-12-31 03:07:41', '2020-12-31 03:07:41'),
(2, 1, 'excerpt', 'apartment', '2020-12-31 03:07:41', '2020-12-31 03:07:41'),
(3, 2, 'credit_charge', '150', '2020-12-31 03:08:00', '2020-12-31 03:08:00'),
(4, 2, 'excerpt', 'villa', '2020-12-31 03:08:00', '2020-12-31 03:08:00'),
(5, 3, 'credit_charge', '120', '2020-12-31 03:08:14', '2020-12-31 03:08:14'),
(6, 3, 'excerpt', 'Houses', '2020-12-31 03:08:14', '2021-06-03 10:14:43'),
(7, 4, 'credit_charge', '120', '2020-12-31 03:08:30', '2020-12-31 03:08:30'),
(8, 4, 'excerpt', 'Condo', '2020-12-31 03:08:30', '2020-12-31 03:08:30'),
(9, 5, 'credit_charge', '100', '2020-12-31 03:08:49', '2020-12-31 03:08:49'),
(10, 5, 'excerpt', 'land', '2020-12-31 03:08:49', '2020-12-31 03:08:49'),
(11, 6, 'credit_charge', '120', '2020-12-31 03:09:13', '2020-12-31 03:09:13'),
(12, 6, 'excerpt', 'New Developments', '2020-12-31 03:09:13', '2021-06-03 10:15:41'),
(13, 14, 'position', 'left', '2020-12-31 03:16:50', '2020-12-31 03:16:50'),
(14, 19, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116102894065ffb10fe25db5.webp', '2020-12-31 03:24:25', '2021-01-10 09:37:13'),
(15, 19, 'mapinfo', '{\"latitude\":\"5.9586\",\"longitude\":\"101475\",\"zoom\":\"10\"}', '2020-12-31 03:24:25', '2021-06-03 10:50:20'),
(16, 20, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116102894135ffb11055db3b.webp', '2020-12-31 03:25:23', '2021-01-10 09:36:58'),
(17, 20, 'mapinfo', '{\"latitude\":\"4.1593\",\"longitude\":\"9.2435\",\"zoom\":\"10\"}', '2020-12-31 03:25:23', '2021-06-03 10:52:02'),
(18, 21, 'preview', '', '2020-12-31 03:27:36', '2020-12-31 03:27:36'),
(19, 21, 'mapinfo', '{\"latitude\":\"23.8103\",\"longitude\":\"90.4125\",\"zoom\":\"10\"}', '2020-12-31 03:27:36', '2020-12-31 03:27:36'),
(20, 22, 'preview', '', '2020-12-31 03:28:31', '2020-12-31 03:28:31'),
(21, 22, 'mapinfo', '{\"latitude\":\"23.6019\",\"longitude\":\"89.8333\",\"zoom\":\"10\"}', '2020-12-31 03:28:31', '2020-12-31 03:28:31'),
(22, 23, 'preview', '', '2020-12-31 03:29:17', '2020-12-31 03:29:17'),
(23, 23, 'mapinfo', '{\"latitude\":\"22.3569\",\"longitude\":\"91.7282\",\"zoom\":\"10\"}', '2020-12-31 03:29:17', '2020-12-31 03:29:17'),
(24, 24, 'preview', '', '2020-12-31 03:30:51', '2020-12-31 03:30:51'),
(25, 24, 'mapinfo', '{\"latitude\":\"21.4272\",\"longitude\":\"92.0058\",\"zoom\":\"10\"}', '2020-12-31 03:30:51', '2020-12-31 03:30:51'),
(26, 31, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116102905585ffb157edfcf8.webp', '2021-01-10 09:56:28', '2021-01-10 09:56:28'),
(27, 31, 'mapinfo', '{\"latitude\":\"3.8441\",\"longitude\":\"11.5013\",\"zoom\":\"10\"}', '2021-01-10 09:56:28', '2021-06-03 10:53:00'),
(28, 32, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116102905605ffb158005816.webp', '2021-01-10 09:57:58', '2021-01-10 09:57:58'),
(29, 32, 'mapinfo', '{\"latitude\":\"4.0615\",\"longitude\":\"9.7860\",\"zoom\":\"10\"}', '2021-01-10 09:57:58', '2021-06-03 10:54:05'),
(30, 33, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116102905595ffb157f46dcc.webp', '2021-01-10 09:59:05', '2021-01-10 09:59:05'),
(31, 33, 'mapinfo', '{\"latitude\":\"22.8456\",\"longitude\":\"89.5403\",\"zoom\":\"10\"}', '2021-01-10 09:59:05', '2021-01-10 09:59:05'),
(32, 34, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116102905595ffb157fa23a5.webp', '2021-01-10 10:02:50', '2021-01-10 10:04:15'),
(33, 34, 'mapinfo', '{\"latitude\":\"24.8949\",\"longitude\":\"91.8687\",\"zoom\":\"10\"}', '2021-01-10 10:02:50', '2021-01-10 10:02:50'),
(34, 35, 'icon', 'fas fa-wifi', '2021-01-10 12:53:51', '2021-01-10 12:53:51'),
(35, 36, 'icon', 'fas fa-swimmer', '2021-01-10 12:54:12', '2021-01-10 12:54:12'),
(36, 37, 'icon', 'fas fa-car-side', '2021-01-10 12:54:37', '2021-01-10 12:54:37'),
(37, 38, 'icon', 'fas fa-user-secret', '2021-01-10 12:54:57', '2021-01-10 12:54:57'),
(38, 39, 'icon', 'fas fa-hand-holding-heart', '2021-01-10 12:55:39', '2021-01-10 12:55:39'),
(39, 40, 'icon', 'fas fa-archway', '2021-01-10 12:56:00', '2021-01-10 12:56:00'),
(40, 41, 'icon', 'far fa-hospital', '2021-01-10 13:01:58', '2021-01-10 13:01:58'),
(41, 42, 'icon', 'fas fa-shopping-cart', '2021-01-10 13:02:45', '2021-01-10 13:02:45'),
(42, 43, 'icon', 'fas fa-school', '2021-01-10 13:03:04', '2021-01-10 13:03:04'),
(43, 44, 'icon', 'fas fa-align-center', '2021-01-10 13:03:20', '2021-01-10 13:03:20'),
(44, 45, 'icon', 'fas fa-hand-holding-heart', '2021-01-10 13:04:00', '2021-01-10 13:04:00'),
(45, 46, 'icon', 'fab fa-angrycreative', '2021-01-10 13:04:23', '2021-01-10 13:04:23'),
(46, 47, 'icon', 'fas fa-train', '2021-01-10 13:04:35', '2021-01-10 13:04:35'),
(47, 48, 'icon', 'fas fa-bus', '2021-01-10 13:04:50', '2021-01-10 13:04:50'),
(48, 49, 'icon', 'fas fa-umbrella-beach', '2021-01-10 13:05:05', '2021-01-10 13:05:05'),
(49, 50, 'icon', 'fas fa-luggage-cart', '2021-01-10 13:05:24', '2021-01-10 13:05:24'),
(50, 51, 'icon', 'fas fa-money-bill', '2021-01-10 13:05:40', '2021-01-10 13:05:40'),
(51, 17, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103399535ffbd67132cff.webp', '2021-01-10 23:39:26', '2021-01-10 23:39:26'),
(52, 16, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103399535ffbd671f06e5.webp', '2021-01-10 23:39:50', '2021-01-10 23:39:50'),
(53, 15, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103399535ffbd671f06e5.webp', '2021-01-10 23:44:08', '2021-01-10 23:44:08'),
(54, 52, 'credit', NULL, '2021-01-11 09:53:21', '2021-01-11 09:53:21'),
(55, 52, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103778575ffc6a81e5911.webp', '2021-01-11 09:53:21', '2021-01-11 10:11:04'),
(56, 52, 'content', '{\"address\":\"Dhaka, Savar\",\"phone\":\"096545345345\",\"description\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\",\"facebook\":\"#\",\"twitter\":\"#\",\"youtube\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"#\",\"instagram\":\"#\",\"whatsapp\":\"2342342342342\",\"service_area\":\"Dhaka, Khulna, Savar\",\"tax_number\":\"35235325353\",\"license\":\"32432523532532\",\"email\":\"support@email.com\"}', '2021-01-11 09:53:21', '2021-01-11 09:53:21'),
(57, 53, 'credit', NULL, '2021-01-11 10:23:26', '2021-01-11 10:23:26'),
(58, 53, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103785955ffc6d635efd4.webp', '2021-01-11 10:23:26', '2021-01-11 10:23:26'),
(59, 53, 'content', '{\"address\":\"Agrabad, Chittagong\",\"phone\":\"045345345345345\",\"description\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\",\"facebook\":\"#\",\"twitter\":\"#\",\"youtube\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"#\",\"instagram\":\"#\",\"whatsapp\":\"547457457457\",\"service_area\":\"Naogaon, Dhaka, Kirtipur\",\"tax_number\":\"43653463463463\",\"license\":\"353456345353535\",\"email\":\"support@morden.com\"}', '2021-01-11 10:23:26', '2021-01-11 10:23:26'),
(60, 54, 'credit', NULL, '2021-01-11 10:31:19', '2021-01-11 10:31:19'),
(61, 54, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103785945ffc6d62d6210.webp', '2021-01-11 10:31:19', '2021-01-11 10:31:19'),
(62, 54, 'content', '{\"address\":\"Naogaon, Kirtipur\",\"phone\":\"034324324324324\",\"description\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\",\"facebook\":\"#\",\"twitter\":\"#\",\"youtube\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"#\",\"instagram\":\"#\",\"whatsapp\":\"#\",\"service_area\":\"Naogaon, Kirtipur, Mohadebpur\",\"tax_number\":\"345435345325325\",\"license\":\"3532532532532532523\",\"email\":\"support@company.com\"}', '2021-01-11 10:31:19', '2021-01-11 10:31:19'),
(69, 61, 'lang', '{\"lang_name\":\"English\",\"lang_position\":\"LTR\"}', '2021-01-18 04:22:05', '2021-01-18 04:22:05'),
(70, 62, 'lang', '{\"lang_name\":\"Arabic\",\"lang_position\":\"RTL\"}', '2021-01-18 04:22:14', '2021-01-18 04:22:14'),
(71, 63, 'position', 'right', '2020-12-31 03:16:50', '2020-12-31 03:16:50'),
(72, 64, 'credentials', '{\"client_id\":\"\",\"client_secret\":\"\",\"currency\":\"USD\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(73, 65, 'credentials', '{\"publishable_key\":\"\",\"secret_key\":\"\",\"currency\":\"USD\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(74, 66, 'credentials', '{\"userSecretKey\":\"\",\"categoryCode\":\"\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(75, 67, 'credentials', '{\"key_id\":\"\",\"key_secret\":\"\",\"currency\":\"USD\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(76, 68, 'credentials', '{\"x_api_Key\":\"\",\"x_api_token\":\"\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(77, 69, 'credentials', '{\"api_key\":\"\",\"currency\":\"USD\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(78, 70, 'preview', '', '2021-06-03 10:58:56', '2021-06-03 10:58:56'),
(79, 70, 'mapinfo', '{\"latitude\":\"22.35\",\"longitude\":\"91.35\",\"zoom\":\"10\"}', '2021-06-03 10:58:56', '2021-06-03 10:58:56'),
(80, 71, 'preview', '', '2021-06-03 10:59:54', '2021-06-03 10:59:54'),
(81, 71, 'mapinfo', '{\"latitude\":\"12\",\"longitude\":\"32\",\"zoom\":\"10\"}', '2021-06-03 10:59:54', '2021-06-03 10:59:54'),
(82, 72, 'preview', '', '2021-06-03 11:00:12', '2021-06-03 11:00:12'),
(83, 72, 'mapinfo', '{\"latitude\":\"43\",\"longitude\":\"32\",\"zoom\":\"10\"}', '2021-06-03 11:00:12', '2021-06-03 11:00:12'),
(84, 73, 'preview', '', '2021-06-03 11:00:23', '2021-06-03 11:00:23'),
(85, 73, 'mapinfo', '{\"latitude\":\"21\",\"longitude\":\"43\",\"zoom\":\"10\"}', '2021-06-03 11:00:23', '2021-06-03 11:00:23'),
(86, 74, 'preview', '', '2021-06-03 11:00:39', '2021-06-03 11:00:39'),
(87, 74, 'mapinfo', '{\"latitude\":\"12\",\"longitude\":\"43\",\"zoom\":\"10\"}', '2021-06-03 11:00:39', '2021-06-03 11:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `categoryrelations`
--

CREATE TABLE `categoryrelations` (
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `child_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categoryrelations`
--

INSERT INTO `categoryrelations` (`parent_id`, `child_id`) VALUES
(18, 4),
(17, 1),
(17, 2),
(17, 3),
(17, 4),
(17, 5),
(17, 6),
(16, 1),
(16, 2),
(16, 3),
(16, 4),
(16, 5),
(16, 6),
(15, 6);

-- --------------------------------------------------------

--
-- Table structure for table `categoryusers`
--

CREATE TABLE `categoryusers` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categoryusers`
--

INSERT INTO `categoryusers` (`category_id`, `user_id`) VALUES
(52, 2),
(52, 3),
(52, 4),
(53, 2),
(53, 3),
(53, 4),
(54, 2),
(54, 3),
(54, 4);

-- --------------------------------------------------------

--
-- Table structure for table `customizers`
--

CREATE TABLE `customizers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customizers`
--

INSERT INTO `customizers` (`id`, `key`, `theme_name`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, 'property_agents', 'jomidar', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":\"Proportioned interiors Club over 10,000 square feet\",\"new_value\":\"Our team of Experienced Property Agents are ready to help\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2020-11-20 04:09:16', '2021-06-16 11:35:09'),
(2, 'hero', 'jomidar', '{\"settings\":{\"hero_big_title\":{\"old_value\":\"Best Place To Find Dream Home.\",\"new_value\":\"Best Place to Find Buy  Property in Cameroon\"},\"hero_des\":{\"old_value\":\"From as low as $10 per day with limited time offer discounts.\",\"new_value\":\"Experienced team of Architects, property agents and lawyers\"},\"hero_address_placeholder\":{\"old_value\":null,\"new_value\":\"Enter keyword\"},\"hero_search_btn\":{\"old_value\":null,\"new_value\":\"Search\"},\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2020-11-20-5fb7880d921db.jpg\"}}}', '1', '2020-11-20 04:10:04', '2021-06-02 15:32:40'),
(3, 'header', 'jomidar', '{\"settings\":{\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2020-11-20-5fb78818a17c8.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"},\"header_phone_number\":{\"old_value\":\"+945253322\",\"new_value\":\"+0433423242331\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"}}}', '1', '2020-11-20 04:10:48', '2021-01-21 04:52:53'),
(4, 'featured_properties', 'jomidar', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Featured Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2020-11-20 04:17:55', '2020-11-20 04:18:28'),
(5, 'find_city', 'jomidar', '{\"settings\":{\"find_city_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2020-11-20-5fb78ac53aa98.jpg\"},\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in these cities\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2020-11-20 04:22:13', '2020-11-20 04:22:32'),
(6, 'blog_list', 'jomidar', '{\"settings\":{\"blog_list_title\":{\"old_value\":\"All Blogs\",\"new_value\":\"All Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2020-11-20 04:25:44', '2020-11-20 04:26:15'),
(7, 'footer', 'jomidar', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2020-11-20-5fb792e445c04.png\"},\"footer_des\":{\"old_value\":\"Maruf\",\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2020. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2020-11-20 04:56:52', '2021-02-07 04:51:12'),
(8, 'blog_breadcrumb', 'jomidar', '{\"settings\":{\"bg_breadcrumb_img\":{\"old_value\":\"uploads\\/2021-01-12-5ffdd9e633477.jpg\",\"new_value\":\"uploads\\/2021-01-12-5ffddab2e1121.jpg\"},\"breadcrumb_title\":{\"old_value\":null,\"new_value\":\"Blog Lists\"},\"breadcrumb_des\":{\"old_value\":null,\"new_value\":\"Blog Lists\"},\"bg_blog_breadcrumb_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-12-5ffddada9c2c4.jpg\"}}}', '1', '2021-01-12 12:18:30', '2021-01-12 12:22:36'),
(9, 'agency_list_breadcrumb', 'jomidar', '{\"settings\":{\"bg_breadcrumb_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-12-5ffdda0a49b41.jpg\"},\"breadcrumb_title\":{\"old_value\":null,\"new_value\":\"Agency Lists\"},\"breadcrumb_des\":{\"old_value\":null,\"new_value\":\"Agency Lists\"},\"breadcrumb_agency_title\":{\"old_value\":null,\"new_value\":\"All Agency\"},\"breadcrumb_agency_des\":{\"old_value\":null,\"new_value\":\"All Agency\"}}}', '1', '2021-01-12 12:19:06', '2021-01-20 19:15:11'),
(10, 'breadcrumb', 'jomidar', '{\"settings\":{\"bg_breadcrumb_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-12-5ffdda1eb1adb.jpg\"},\"breadcrumb_title\":{\"old_value\":null,\"new_value\":\"Agent Lists\"},\"breadcrumb_des\":{\"old_value\":null,\"new_value\":\"Agent Lists\"},\"breadcrumb_agent_title\":{\"old_value\":null,\"new_value\":\"All Agents\"},\"breadcrumb_agent_des\":{\"old_value\":null,\"new_value\":\"All Agents\"}}}', '1', '2021-01-12 12:19:26', '2021-01-20 19:14:49'),
(11, 'header', 'zamindar', '{\"settings\":{\"header_phone_number\":{\"old_value\":\"4354354364443\",\"new_value\":\"435435436444\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-24-600d4506f0667.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-01-24 10:58:44', '2021-01-24 11:00:32'),
(12, 'featured_properties', 'zamindar', '{\"settings\":{\"featured_properties_title\":{\"old_value\":\"Latest Property\",\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-01-24 11:00:57', '2021-01-24 11:01:25'),
(13, 'blog_list', 'zamindar', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-24 11:01:46', '2021-01-24 11:02:04'),
(14, 'footer', 'zamindar', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-24-600d45bb7e54d.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Jomidar - 2020. Design By Amcoders\"}},\"content\":{\"\":{\"social_facebook_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_twitter_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_google_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_instagram_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_pinterest_link\":{\"old_value\":null,\"new_value\":\"#\"}},\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-24 11:02:35', '2021-01-24 11:03:48'),
(15, 'hero', 'zamindar', '{\"settings\":{\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-24-600da81638203.jpg\"},\"hero_big_title\":{\"old_value\":null,\"new_value\":\"Fast way to achieve your goals in business\"},\"hero_des\":{\"old_value\":null,\"new_value\":\"To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine occidental\"},\"hero_btn_title\":{\"old_value\":null,\"new_value\":\"Learn More\"},\"hero_btn_link\":{\"old_value\":null,\"new_value\":\"#\"},\"hero_address_placeholder\":{\"old_value\":null,\"new_value\":\"Enter Your Address\"},\"hero_search_btn\":{\"old_value\":null,\"new_value\":\"Search\"}}}', '1', '2021-01-24 18:02:14', '2021-01-24 18:03:36'),
(16, 'find_city', 'zamindar', '{\"settings\":{\"find_city_title\":{\"old_value\":\"Find us in your city\",\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":\"Handpicked Exclusive Properties By Our Team.\",\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"city_btn_title\":{\"old_value\":null,\"new_value\":\"View All City\"}}}', '1', '2021-01-24 18:23:20', '2021-01-24 18:24:44'),
(17, 'property_agents', 'zamindar', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-01-24 18:37:51', '2021-01-24 18:38:15'),
(18, 'counter', 'zamindar', '{\"content\":{\"counter_first_section\":{\"counter_first_section_title\":{\"old_value\":\"to\",\"new_value\":\"Total Agents\"},\"counter_first_section_count\":{\"old_value\":null,\"new_value\":\"880\"}},\"counter_second_section\":{\"counter_second_section_title\":{\"old_value\":null,\"new_value\":\"Total Sales\"},\"counter_second_section_count\":{\"old_value\":null,\"new_value\":\"1798\"}},\"counter_third_section\":{\"counter_third_section_title\":{\"old_value\":null,\"new_value\":\"Total Projects\"},\"counter_third_section_count\":{\"old_value\":null,\"new_value\":\"582\"}},\"counter_four_section\":{\"counter_four_section_title\":{\"old_value\":null,\"new_value\":\"Total Customers\"},\"counter_four_section_count\":{\"old_value\":null,\"new_value\":\"3698\"}}},\"settings\":{\"counter_bg_img\":{\"old_value\":\"uploads\\/2021-01-25-600e4f6bf146a.png\",\"new_value\":\"uploads\\/2021-01-25-600e4fa1d2490.jpg\"}}}', '1', '2021-01-25 05:50:05', '2021-01-25 05:58:05'),
(19, 'header', 'bari', '{\"settings\":{\"header_phone_number\":{\"old_value\":\"+97867855656\",\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":\"support@amcod\",\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":\"uploads\\/2021-01-27-601191c16b1fd.png\",\"new_value\":\"uploads\\/2021-01-27-601191c5582e8.png\"},\"header_create_property_title\":{\"old_value\":\"Create Property\",\"new_value\":\"Create Property\"}}}', '1', '2021-01-27 17:11:34', '2021-01-27 17:16:39'),
(20, 'hero', 'bari', '{\"settings\":{\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-27-60119119268d7.jpg\"},\"hero_big_title\":{\"old_value\":null,\"new_value\":\"Discover Your Best Place to Live\"},\"hero_des\":{\"old_value\":null,\"new_value\":\"To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine occidental\"},\"hero_first_btn_title\":{\"old_value\":null,\"new_value\":\"Learn More\"},\"hero_second_btn_title\":{\"old_value\":null,\"new_value\":\"View Details\"},\"hero_first_btn_link\":{\"old_value\":null,\"new_value\":\"#\"},\"hero_second_btn_link\":{\"old_value\":null,\"new_value\":\"#\"}}}', '1', '2021-01-27 17:13:13', '2021-01-27 17:14:06'),
(21, 'featured_properties', 'bari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-01-27 17:16:52', '2021-01-27 17:17:17'),
(22, 'property_agents', 'bari', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-01-27 17:17:55', '2021-01-27 17:18:21'),
(23, 'find_city', 'bari', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-01-27 17:18:27', '2021-01-27 17:18:47'),
(24, 'blog_list', 'bari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-27 17:19:00', '2021-01-27 17:19:12'),
(25, 'footer', 'bari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-27-6011928987ddb.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2020. Design By Amcoders\"}},\"content\":{\"\":{\"social_facebook_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_twitter_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_google_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_instagram_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_pinterest_link\":{\"old_value\":null,\"new_value\":\"#\"}},\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-27 17:19:21', '2021-01-27 17:20:15'),
(26, 'hero', 'hazibari', '{\"settings\":{\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-29-601412e0f0233.jpg\"},\"hero_big_title\":{\"old_value\":null,\"new_value\":\"Find Your Future Home\"}}}', '1', '2021-01-29 14:51:29', '2021-01-29 14:51:39'),
(27, 'find_city', 'hazibari', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-01-29 14:56:22', '2021-01-29 14:56:34'),
(28, 'blog_list', 'hazibari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-29 15:05:25', '2021-01-29 15:07:49'),
(29, 'footer', 'hazibari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-29-6014164031f32.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2020. Design By Amcoders\"}},\"content\":{\"\":{\"social_facebook_link\":{\"old_value\":\"#ede\",\"new_value\":\"fgfg\"},\"social_twitter_link\":{\"old_value\":\"#\",\"new_value\":\"dfdfdsf\"},\"social_google_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_instagram_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_pinterest_link\":{\"old_value\":null,\"new_value\":\"#\"}},\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-29 15:05:52', '2021-01-29 15:09:07'),
(30, 'featured_properties', 'hazibari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-01-29 15:14:56', '2021-01-29 15:15:15'),
(31, 'review', 'hazibari', '{\"settings\":{\"review_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-29-601419b6c2696.png\"},\"review_title\":{\"old_value\":null,\"new_value\":\"Good Reviews By Clients\"},\"review_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.\"}}}', '1', '2021-01-29 15:20:38', '2021-01-29 15:21:17'),
(32, 'header', 'thakorbari', '{\"settings\":{\"header_phone_number\":{\"old_value\":null,\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":\"uploads\\/2021-01-30-6015670368313.png\",\"new_value\":\"uploads\\/2021-01-30-60156786463dc.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-01-30 15:02:09', '2021-01-30 15:05:00'),
(33, 'hero', 'thakorbari', '{\"settings\":{\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-30-60156797f0bab.png\"},\"hero_big_title\":{\"old_value\":null,\"new_value\":\"GET A PROPERTY NOW @ AFFORDABLE PRICE\"},\"hero_des\":{\"old_value\":null,\"new_value\":\"Making Real Estate Simple & Easier for your Business or your Family\"},\"hero_address_placeholder\":{\"old_value\":null,\"new_value\":\"Location\"},\"hero_search_btn\":{\"old_value\":null,\"new_value\":\"Search\"}}}', '1', '2021-01-30 15:05:12', '2021-01-30 15:05:49'),
(34, 'featured_properties', 'thakorbari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-01-30 15:10:31', '2021-01-30 15:13:11'),
(35, 'counter', 'thakorbari', '{\"settings\":{\"counter_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-30-601568fe127bc.jpg\"}},\"content\":{\"counter_first_section\":{\"counter_first_section_title\":{\"old_value\":null,\"new_value\":\"Total Agents\"},\"counter_first_section_count\":{\"old_value\":null,\"new_value\":\"880\"}},\"counter_second_section\":{\"counter_second_section_title\":{\"old_value\":null,\"new_value\":\"Total Sales\"},\"counter_second_section_count\":{\"old_value\":null,\"new_value\":\"1798\"}},\"counter_third_section\":{\"counter_third_section_title\":{\"old_value\":null,\"new_value\":\"Total Projects\"},\"counter_third_section_count\":{\"old_value\":null,\"new_value\":\"582\"}},\"counter_four_section\":{\"counter_four_section_title\":{\"old_value\":null,\"new_value\":\"Total Customers\"},\"counter_four_section_count\":{\"old_value\":null,\"new_value\":\"3698\"}}}}', '1', '2021-01-30 15:11:10', '2021-01-30 15:13:11'),
(36, 'blog_list', 'thakorbari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-30 15:11:52', '2021-01-30 15:13:11'),
(37, 'footer', 'thakorbari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-30-60156938d54e6.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2020. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-30 15:12:08', '2021-01-30 15:13:12'),
(38, 'property_agents', 'thakorbari', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-01-30 15:20:34', '2021-01-30 15:21:04'),
(39, 'header', 'amarbari', '{\"settings\":{\"header_phone_number\":{\"old_value\":\"4354354364443\",\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":\"support@amcoders.com\",\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":\"uploads\\/2021-01-30-60156ed56440f.png\",\"new_value\":\"uploads\\/2021-01-30-60156ee761243.png\"},\"header_create_property_title\":{\"old_value\":\"Create Property\",\"new_value\":\"Create Property\"}}}', '1', '2021-01-30 15:35:55', '2021-01-30 15:36:27'),
(40, 'hero', 'amarbari', '{\"settings\":{\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-30-60156f299d0ba.jpg\"},\"hero_big_title\":{\"old_value\":null,\"new_value\":\"Best Place To Live\"},\"hero_des\":{\"old_value\":null,\"new_value\":\"In the heart of Brooklyn, in a vibrant neighborhood just east of Prospect Park, stands an eight-story, full-service, strikingly beautiful apartment building\"},\"hero_first_btn_title\":{\"old_value\":null,\"new_value\":\"Learn More\"},\"hero_first_btn_link\":{\"old_value\":null,\"new_value\":\"#\"}}}', '1', '2021-01-30 15:37:29', '2021-01-30 15:38:00'),
(41, 'featured_properties', 'amarbari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-01-30 15:38:05', '2021-01-30 15:39:43'),
(42, 'find_city', 'amarbari', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-01-30 15:38:30', '2021-01-30 15:39:43'),
(43, 'property_agents', 'amarbari', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-01-30 15:38:39', '2021-01-30 15:39:43'),
(44, 'blog_list', 'amarbari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-30 15:38:50', '2021-01-30 15:39:43'),
(45, 'footer', 'amarbari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-30-60156f85e77bb.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-30 15:39:02', '2021-01-30 15:39:43'),
(46, 'header', 'marufbari', '{\"settings\":{\"header_phone_number\":{\"old_value\":null,\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-6016435bf23c8.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-01-31 06:42:46', '2021-01-31 06:45:07'),
(47, 'hero', 'marufbari', '{\"settings\":{\"hero_big_title\":{\"old_value\":\"GET A PROPERTY NOW @ AFFORDABLE PRICE\",\"new_value\":\"Find the good out there.\"},\"hero_search_btn\":{\"old_value\":null,\"new_value\":\"Search\"}}}', '1', '2021-01-31 06:43:00', '2021-01-31 06:45:07'),
(48, 'featured_properties', 'marufbari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-01-31 06:43:21', '2021-01-31 06:45:07'),
(49, 'counter', 'marufbari', '{\"content\":{\"counter_first_section\":{\"counter_first_section_title\":{\"old_value\":null,\"new_value\":\"Total Agents\"},\"counter_first_section_count\":{\"old_value\":null,\"new_value\":\"880\"}},\"counter_second_section\":{\"counter_second_section_title\":{\"old_value\":null,\"new_value\":\"Total Sales\"},\"counter_second_section_count\":{\"old_value\":null,\"new_value\":\"1798\"}},\"counter_third_section\":{\"counter_third_section_title\":{\"old_value\":null,\"new_value\":\"Total Projects\"},\"counter_third_section_count\":{\"old_value\":null,\"new_value\":\"582\"}},\"counter_four_section\":{\"counter_four_section_title\":{\"old_value\":null,\"new_value\":\"Total Customers\"},\"counter_four_section_count\":{\"old_value\":null,\"new_value\":\"3698\"}}},\"settings\":{\"counter_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-6016439283281.jpg\"}}}', '1', '2021-01-31 06:43:46', '2021-01-31 06:45:07'),
(50, 'find_city', 'marufbari', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-01-31 06:44:03', '2021-01-31 06:45:07'),
(51, 'blog_list', 'marufbari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-31 06:44:14', '2021-01-31 06:45:07'),
(52, 'footer', 'marufbari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-601643ba73deb.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2020. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-31 06:44:26', '2021-01-31 06:45:07'),
(53, 'hero', 'arafatbari', '{\"settings\":{\"hero_background_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-60168e1be3e26.jpg\"},\"hero_big_title\":{\"old_value\":null,\"new_value\":\"Find Your Dream Property\"},\"hero_des\":{\"old_value\":null,\"new_value\":\"We have lots of properties in various locations available for purchase.\"},\"hero_address_placeholder\":{\"old_value\":null,\"new_value\":\"Location\"},\"hero_search_btn\":{\"old_value\":null,\"new_value\":\"Search\"}}}', '1', '2021-01-31 12:01:48', '2021-01-31 12:02:59'),
(54, 'header', 'arafatbari', '{\"settings\":{\"header_phone_number\":{\"old_value\":null,\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-60168e4ade2a3.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-01-31 12:02:25', '2021-01-31 12:02:59'),
(55, 'featured_properties', 'arafatbari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-01-31 12:02:44', '2021-01-31 12:02:59'),
(56, 'find_city', 'arafatbari', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-01-31 12:03:09', '2021-01-31 12:03:14'),
(57, 'review', 'arafatbari', '{\"settings\":{\"review_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-60168e7d04c0e.png\"},\"review_title\":{\"old_value\":null,\"new_value\":\"Good Reviews By Clients\"},\"review_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.\"}}}', '1', '2021-01-31 12:03:25', '2021-01-31 12:04:55'),
(58, 'blog_list', 'arafatbari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-01-31 12:03:54', '2021-01-31 12:04:55'),
(59, 'footer', 'arafatbari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-01-31-60168eafa6db7.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2020. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-01-31 12:04:15', '2021-01-31 12:04:55'),
(60, 'header', 'hellobari', '{\"settings\":{\"header_phone_number\":{\"old_value\":null,\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-06-601d8afbf1ff1.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-02-05 19:14:07', '2021-02-05 19:14:26'),
(61, 'hero', 'hellobari', '{\"settings\":{\"hero_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-22-6033df3a185af.jpg\"}}}', '1', '2021-02-22 17:43:38', '2021-02-22 17:46:23'),
(62, 'featured_properties', 'hellobari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-02-22 17:43:49', '2021-02-22 17:46:23'),
(63, 'counter', 'hellobari', '{\"settings\":{\"counter_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-22-6033df6368137.jpg\"}},\"content\":{\"counter_first_section\":{\"counter_first_section_title\":{\"old_value\":null,\"new_value\":\"Total Agents\"},\"counter_first_section_count\":{\"old_value\":null,\"new_value\":\"880\"}},\"counter_second_section\":{\"counter_second_section_title\":{\"old_value\":null,\"new_value\":\"Total Sales\"},\"counter_second_section_count\":{\"old_value\":null,\"new_value\":\"1798\"}},\"counter_third_section\":{\"counter_third_section_title\":{\"old_value\":null,\"new_value\":\"Total Projects\"},\"counter_third_section_count\":{\"old_value\":null,\"new_value\":\"582\"}},\"counter_four_section\":{\"counter_four_section_title\":{\"old_value\":null,\"new_value\":\"Total Customers\"},\"counter_four_section_count\":{\"old_value\":null,\"new_value\":\"3698\"}}}}', '1', '2021-02-22 17:44:19', '2021-02-22 17:46:23'),
(64, 'property_agents', 'hellobari', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-02-22 17:44:38', '2021-02-22 17:46:23'),
(65, 'find_city', 'hellobari', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-02-22 17:44:46', '2021-02-22 17:46:23'),
(66, 'blog_list', 'hellobari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-02-22 17:44:53', '2021-02-22 17:46:23'),
(67, 'footer', 'hellobari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-22-6033df938d691.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2021. Design By Amcoders\"}},\"content\":{\"\":{\"social_facebook_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_twitter_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_google_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_instagram_link\":{\"old_value\":null,\"new_value\":\"#\"},\"social_pinterest_link\":{\"old_value\":null,\"new_value\":\"#\"}},\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-02-22 17:45:07', '2021-02-22 17:46:23'),
(68, 'header', 'mamabari', '{\"settings\":{\"header_phone_number\":{\"old_value\":\"4354354364443\",\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":\"support@amcoders.com\",\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":\"uploads\\/2021-02-24-6035d10068988.png\",\"new_value\":\"uploads\\/2021-02-24-6035d17d6913b.png\"},\"header_signin_title\":{\"old_value\":\"Sign In\",\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":\"Create Property\",\"new_value\":\"Create Property\"}}}', '1', '2021-02-24 05:07:21', '2021-02-24 05:09:39'),
(69, 'hero', 'mamabari', '{\"settings\":{\"hero_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-24-6035d1a278a47.jpg\"},\"hero_title\":{\"old_value\":null,\"new_value\":\"Do More With Jomidar\"}}}', '1', '2021-02-24 05:10:10', '2021-02-24 05:10:35'),
(70, 'property_agents', 'mamabari', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-02-24 05:11:28', '2021-02-24 05:11:42'),
(71, 'featured_properties', 'mamabari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-02-24 05:12:03', '2021-02-24 05:12:10'),
(72, 'find_city', 'mamabari', '{\"settings\":{\"find_city_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-24-6035d2227c1c2.jpg\"},\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"}}}', '1', '2021-02-24 05:12:18', '2021-02-24 05:12:26'),
(73, 'blog_list', 'mamabari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-02-24 05:12:42', '2021-02-24 05:12:49'),
(74, 'footer', 'mamabari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-24-6035d24c0e1bd.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2021. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-02-24 05:13:00', '2021-02-24 05:13:29'),
(75, 'header', 'nanabari', '{\"settings\":{\"header_phone_number\":{\"old_value\":null,\"new_value\":\"4354354364443\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-24-60367a3f9b2d1.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-02-24 17:09:05', '2021-02-24 17:09:42'),
(76, 'property_agents', 'nanabari', '{\"settings\":{\"property_agent_title\":{\"old_value\":null,\"new_value\":\"Property Agents\"},\"property_agent_descrtiption\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"property_agent_btn_title\":{\"old_value\":null,\"new_value\":\"View All Agents\"}}}', '1', '2021-02-24 17:09:49', '2021-02-24 17:10:39'),
(77, 'featured_properties', 'nanabari', '{\"settings\":{\"featured_properties_title\":{\"old_value\":null,\"new_value\":\"Latest Properties\"},\"featured_properties_des\":{\"old_value\":null,\"new_value\":\"Proportioned interiors Club over 10,000 square feet\"},\"featured_properties_btn_title\":{\"old_value\":null,\"new_value\":\"View All Properties\"}}}', '1', '2021-02-24 17:10:12', '2021-02-24 17:10:39'),
(78, 'find_city', 'nanabari', '{\"settings\":{\"find_city_bg_img\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-24-60367a71f15ca.jpg\"},\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in your city\"},\"find_city_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"}}}', '1', '2021-02-24 17:10:26', '2021-02-24 17:10:39'),
(79, 'blog_list', 'nanabari', '{\"settings\":{\"blog_list_title\":{\"old_value\":null,\"new_value\":\"Latest Blogs\"},\"blog_list_des\":{\"old_value\":null,\"new_value\":\"Handpicked Exclusive Properties By Our Team.\"},\"blog_list_btn_title\":{\"old_value\":null,\"new_value\":\"View All Blogs\"}}}', '1', '2021-02-24 17:10:44', '2021-02-24 17:10:50'),
(80, 'footer', 'nanabari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-24-60367a95a935c.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2021. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-02-24 17:11:01', '2021-02-24 17:11:23'),
(81, 'header', 'tomarbari', '{\"settings\":{\"header_phone_number\":{\"old_value\":null,\"new_value\":\"+880-258-5874\"},\"header_email_address\":{\"old_value\":null,\"new_value\":\"support@amcoders.com\"},\"logo\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-28-603ba542585db.png\"},\"header_signin_title\":{\"old_value\":null,\"new_value\":\"Sign In\"},\"header_create_property_title\":{\"old_value\":null,\"new_value\":\"Create Property\"}}}', '1', '2021-02-28 15:13:59', '2021-02-28 15:14:59'),
(82, 'footer', 'tomarbari', '{\"settings\":{\"footer_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2021-02-28-603ba56bb156d.png\"},\"footer_des\":{\"old_value\":null,\"new_value\":\"Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.\"},\"footer_copyright\":{\"old_value\":null,\"new_value\":\"Copyright \\u00a9 Website - 2021. Design By Amcoders\"}},\"content\":{\"footer_right\":{\"footer_right_title\":{\"old_value\":null,\"new_value\":\"Newsletter\"},\"footer_right_des\":{\"old_value\":null,\"new_value\":\"88 Broklyn Golden Street, New York. USA needhelp@ziston.com\"},\"footer_right_btn_title\":{\"old_value\":null,\"new_value\":\"Subscribe\"}}}}', '1', '2021-02-28 15:15:07', '2021-02-28 15:16:06');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mediaposts`
--

CREATE TABLE `mediaposts` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `media_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mediaposts`
--

INSERT INTO `mediaposts` (`term_id`, `media_id`) VALUES
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 6),
(5, 5),
(6, 11),
(6, 10),
(6, 12),
(6, 13),
(7, 14),
(7, 15),
(7, 16),
(7, 17);

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`id`, `name`, `type`, `url`, `size`, `path`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'uploads/21/01/10012116103007215ffb3d3126706.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116103007215ffb3d3126706.webp', '32184kib', 'uploads/21/01/', 4, '2021-01-10 12:45:21', '2021-01-10 12:45:21'),
(2, 'uploads/21/01/10012116103007215ffb3d3125675.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116103007215ffb3d3125675.webp', '51716kib', 'uploads/21/01/', 4, '2021-01-10 12:45:21', '2021-01-10 12:45:21'),
(3, 'uploads/21/01/10012116103007225ffb3d323297d.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116103007225ffb3d323297d.webp', '22288kib', 'uploads/21/01/', 4, '2021-01-10 12:45:22', '2021-01-10 12:45:22'),
(4, 'uploads/21/01/10012116103007225ffb3d322e80e.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116103007225ffb3d322e80e.webp', '43584kib', 'uploads/21/01/', 4, '2021-01-10 12:45:22', '2021-01-10 12:45:22'),
(5, 'uploads/21/01/10012116103007225ffb3d32f1e81.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116103007225ffb3d32f1e81.webp', '34844kib', 'uploads/21/01/', 4, '2021-01-10 12:45:23', '2021-01-10 12:45:23'),
(6, 'uploads/21/01/10012116103007225ffb3d32e8548.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/10012116103007225ffb3d32e8548.webp', '39370kib', 'uploads/21/01/', 4, '2021-01-10 12:45:23', '2021-01-10 12:45:23'),
(7, 'uploads/21/01/11012116103399535ffbd67132cff.webp', 'png', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103399535ffbd67132cff.webp', '326kib', 'uploads/21/01/', 1, '2021-01-10 23:39:13', '2021-01-10 23:39:13'),
(8, 'uploads/21/01/11012116103399535ffbd671f06e5.webp', 'png', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103399535ffbd671f06e5.webp', '364kib', 'uploads/21/01/', 1, '2021-01-10 23:39:14', '2021-01-10 23:39:14'),
(9, 'uploads/21/01/11012116103399545ffbd672163c9.webp', 'png', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103399545ffbd672163c9.webp', '282kib', 'uploads/21/01/', 1, '2021-01-10 23:39:14', '2021-01-10 23:39:14'),
(10, 'uploads/21/01/11012116103578055ffc1c2dee004.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103578055ffc1c2dee004.webp', '34844kib', 'uploads/21/01/', 3, '2021-01-11 04:36:47', '2021-01-11 04:36:47'),
(11, 'uploads/21/01/11012116103578055ffc1c2dee356.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103578055ffc1c2dee356.webp', '39370kib', 'uploads/21/01/', 3, '2021-01-11 04:36:47', '2021-01-11 04:36:47'),
(12, 'uploads/21/01/11012116103578085ffc1c30c8af4.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103578085ffc1c30c8af4.webp', '10636kib', 'uploads/21/01/', 3, '2021-01-11 04:36:49', '2021-01-11 04:36:49'),
(13, 'uploads/21/01/11012116103578085ffc1c30cc21f.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103578085ffc1c30cc21f.webp', '56598kib', 'uploads/21/01/', 3, '2021-01-11 04:36:49', '2021-01-11 04:36:49'),
(14, 'uploads/21/01/11012116103582515ffc1deba3f0f.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103582515ffc1deba3f0f.webp', '36756kib', 'uploads/21/01/', 2, '2021-01-11 04:44:12', '2021-01-11 04:44:12'),
(15, 'uploads/21/01/11012116103582575ffc1df1cce75.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103582575ffc1df1cce75.webp', '24038kib', 'uploads/21/01/', 2, '2021-01-11 04:44:18', '2021-01-11 04:44:18'),
(16, 'uploads/21/01/11012116103582645ffc1df8df1eb.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103582645ffc1df8df1eb.webp', '56598kib', 'uploads/21/01/', 2, '2021-01-11 04:44:25', '2021-01-11 04:44:25'),
(17, 'uploads/21/01/11012116103582715ffc1dff3d930.webp', 'jpg', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103582715ffc1dff3d930.webp', '51716kib', 'uploads/21/01/', 2, '2021-01-11 04:44:31', '2021-01-11 04:44:31'),
(18, 'uploads/21/06/160621162386144460ca28c41f4c2.webp', 'jpg', '//localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/06/160621162386144460ca28c41f4c2.webp', '20274kib', 'uploads/21/06/', 1, '2021-06-16 11:37:26', '2021-06-16 11:37:26'),
(19, 'uploads/21/06/160621162386152960ca291940b4c.webp', 'jpg', '//localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/06/160621162386152960ca291940b4c.webp', '21078kib', 'uploads/21/06/', 1, '2021-06-16 11:38:50', '2021-06-16 11:38:50'),
(20, 'uploads/21/06/160621162386159960ca295f31433.webp', 'jpeg', '//localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/06/160621162386159960ca295f31433.webp', '179430kib', 'uploads/21/06/', 1, '2021-06-16 11:40:03', '2021-06-16 11:40:03');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `position`, `data`, `lang`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Main Menu', 'Header', '[{\"text\":\"Home\",\"href\":\"/\",\"icon\":\"\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"About Us\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Property Listings\",\"href\":\"/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Buy Property\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Sell Property\",\"href\":\"/map\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Contact\",\"href\":\"/contact\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]', 'en', 1, '2021-01-18 15:39:58', '2021-06-06 10:08:43'),
(5, 'Main Menu', 'Header', '[{\"text\":\"الصفحة الرئيسية\",\"href\":\"/\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"خاصية\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"قائمة الممتلكات\",\"href\":\"/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"خريطة الملكية\",\"href\":\"/map\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"تفاصيل اوضح\",\"href\":\"/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"مشروع\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"المشاريع\",\"href\":\"/project\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"تفاصيل المشروع\",\"href\":\"/project\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"وكيل\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"عملاء\",\"href\":\"/agent/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"تفاصيل الوكيل\",\"href\":\"/agent/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"وكالة\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"وكالات\",\"href\":\"/agency/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"تفاصيل الوكالة\",\"href\":\"/agency/list\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"مدونة\",\"href\":\"#\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"المدونات\",\"href\":\"/blogs\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"تفاصيل المدونة\",\"href\":\"/blogs\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"اتصل\",\"href\":\"/contact\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]', 'ar', 1, '2021-01-18 15:39:58', '2021-01-19 07:50:26'),
(6, 'Explore', 'Footer_left', '[{\"text\":\"About Us\",\"href\":\"/page/about-us\",\"icon\":\"\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Property Listings\",\"href\":\"/agent/property\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Bookmarks\",\"href\":\"/agent/favourites\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]', 'en', 1, '2021-01-20 14:59:56', '2021-06-07 10:09:41'),
(7, 'About', 'Footer_right', '[{\"text\":\"Sitemap\",\"href\":\"/sitemap.xml\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Contact Us\",\"href\":\"/contact\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Terms And Condition\",\"href\":\"/page/terms-and-condition\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]', 'en', 1, '2021-01-20 15:23:41', '2021-06-07 10:11:07'),
(8, 'حول', 'Footer_right', '[{\"text\":\"خريطة الموقع\",\"href\":\"/sitemap.xml\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"اتصل بنا\",\"href\":\"/contact\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"أحكام وشروط\",\"href\":\"/page/terms-and-condition\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"سياسة الخصوصية\",\"href\":\"/page/privacy-policy\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"أحدث الأخبار\",\"href\":\"/blog\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"الدعم\",\"href\":\"/contact\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]', 'ar', 1, '2021-01-20 15:23:41', '2021-01-20 18:04:23'),
(9, 'يكتشف', 'Footer_left', '[{\"text\":\"معلومات عنا\",\"href\":\"/page/about-us\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"حسابي\",\"href\":\"/agent/dashboard\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"القوائم الخاصة بي\",\"href\":\"/agent/property\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"حزم التسعير\",\"href\":\"/agent/package\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"لوحة تحكم المستخدم\",\"href\":\"/agent/dashboard\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"إشارات مرجعية\",\"href\":\"/agent/favourites\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]', 'ar', 1, '2021-01-20 14:59:56', '2021-01-20 18:12:31');

-- --------------------------------------------------------

--
-- Table structure for table `meta`
--

CREATE TABLE `meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'excerpt',
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta`
--

INSERT INTO `meta` (`id`, `term_id`, `type`, `content`) VALUES
(1, 5, 'excerpt', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(2, 5, 'content', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n        \r\n        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>'),
(3, 5, 'contact_type', '{\"contact_type\":\"mail\",\"email\":\"mahi@mailinator.com\"}'),
(4, 5, 'youtube_url', 'wv7_0DtIQps'),
(5, 5, 'virtual_tour', 'https://my.matterport.com/show/?m=CrZgGg34uFa'),
(6, 5, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103007701610300770.jpg\",\"name\":\"First Floor\",\"square_ft\":\"25\"}'),
(7, 5, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103008831610300883.jpg\",\"name\":\"Second Floor\",\"square_ft\":\"65\"}'),
(8, 5, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103009011610300901.jpg\",\"name\":\"Third Floor\",\"square_ft\":\"87\"}'),
(9, 6, 'excerpt', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(10, 6, 'content', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n        \r\n        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>'),
(11, 6, 'contact_type', '{\"contact_type\":\"mail\",\"email\":\"gaqoq@mailinator.com\"}'),
(12, 6, 'youtube_url', 'wv7_0DtIQps'),
(13, 6, 'virtual_tour', 'https://my.matterport.com/show/?m=CrZgGg34uFa'),
(14, 6, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103578331610357833.jpg\",\"name\":\"First Floor\",\"square_ft\":\"23\"}'),
(15, 6, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103578471610357847.jpg\",\"name\":\"Second Floor\",\"square_ft\":\"54\"}'),
(16, 7, 'excerpt', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(17, 7, 'content', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n        \r\n        <p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'),
(18, 7, 'contact_type', '{\"contact_type\":\"mail\",\"email\":\"sicequb@mailinator.com\"}'),
(19, 7, 'youtube_url', 'wv7_0DtIQps'),
(20, 7, 'virtual_tour', 'https://my.matterport.com/show/?m=CrZgGg34uFa'),
(21, 7, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103582891610358289.jpg\",\"name\":\"First Floor\",\"square_ft\":\"43\"}'),
(22, 7, 'floor_plan', '{\"file_name\":\"uploads\\/21\\/01\\/16103584251610358425.jpg\",\"name\":\"Second Floor\",\"square_ft\":\"67\"}'),
(23, 8, 'excerpt', 'Real estate festival is one of the famous feval for explain to you how all this mistaolt deand praising pain wasnad I will give complete'),
(24, 8, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103624465ffc2e4ed8262.webp'),
(25, 8, 'content', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n    \r\n    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>'),
(26, 9, 'excerpt', 'Real estate festival is one of the famous feval for explain to you how all this mistaolt deand praising pain wasnad I will give complete'),
(27, 9, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103624555ffc2e5734666.webp'),
(28, 9, 'content', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n    \r\n    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>'),
(29, 10, 'excerpt', 'Real estate festival is one of the famous feval for explain to you how all this mistaolt deand praising pain wasnad I will give complete'),
(30, 10, 'preview', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103624545ffc2e56c0aa5.webp'),
(31, 10, 'content', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n    \r\n    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>'),
(32, 11, 'excerpt', 'hsvgdhcsvdghcsvdhgcsd'),
(33, 11, 'content', '<h3>Introduction</h3><p>This page states the terms and conditions under which you may use the website&nbsp;<a data-cke-saved-href=\"http://www.peddysproperties.com\" href=\"http://www.peddysproperties.com\">www.peddysproperties.com</a> and all related apps and services. Peddys properties is owned and operated by Peddys Capital (Pty) Ltd, registration number: <strong>2008/019235/07</strong>.</p><p>Our physical address is: Half Mile, Limbe, SW Region, Cameroon</p><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><p><br></p><h3>Definitions</h3><ol><li>The terms \"you\" and \"user\" as used herein refer to all individuals and/or entities accessing the website.</li><li>The term \"website\" as used herein refers to&nbsp;<a data-cke-saved-href=\"http://www.peddysproperties.com\" href=\"http://www.peddysproperties.com\">www.peddysproperties.com</a></li></ol><h3>General</h3><p>By using the website, you are indicating your acceptance to be bound by these terms and conditions. The website may revise these terms and conditions at any time by updating this page. You should visit this page periodically to review the terms and conditions, to which you are bound.</p><h3>Terms of Use</h3><ol><li>Users may not use the website in order to transmit, distribute, store or destroy material:<ul><li>in violation of any applicable law or regulation;</li><li>in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy, publicity or other personal rights of others;</li><li>that is defamatory, obscene, threatening, abusive or hateful.</li></ul></li><li>The following is prohibited with respect to the website:<ul><li>Using any robot, spider, other automatic device or manual process to monitor or copy any part of the website;</li><li>Using any device, software or routine or the like to interfere or attempt to interfere with the proper working of the website.</li><li>Taking any action that imposes an unreasonable or disproportionately large load on the website infrastructure;</li><li>Copying, reproducing, altering, modifying, creating derivative works, or publicly displaying any content from the website without Peddys Properties prior written permission;</li><li>Reverse assembling or otherwise attempting to discover any source code relating to the website or any tool therein, except to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation; and</li><li>Attempting to access any area of the website to which access is not authorized.</li></ul></li></ol><h3>Copyright and Intellectual Property Rights</h3><ol><li>All content, trademarks and data on this website, including but not limited to, software, databases, text, graphics, icons, hyperlinks, private information, and designs are the property of or licensed to the website.</li><li>Users of this website are not granted a licence or any other right including without limitation under Copyright, Trade Mark, Patent or Intellectual Property Rights in/or to the content.</li></ol><h3>Privacy Policy</h3><ol><li>While using this website, information about you may be either collected by us or provided by you. Such information will become the property of Peddys Properties.</li><li>Peddys Properties may use this information to provide you with information regarding our products, services, or events from time-to-time.</li><li>You may request that Peddys Properties cease sending you such information or request that your personal information be removed from our database or mailing list at any time.</li><li>Peddys Properties will not disclose any personal information to anyone except as provided for in this policy. We may however need to disclose personal information to Peddys Properties employees or agents who require such information to carry out their duties. There may also be situations where the law requires us to disclose your personal information. In all other situations, except in the case of the sale of Peddys Properties or its associated services or web site(s), Peddys Properties will not disclose your personal information without giving you prior notice thereof and an opportunity to give your consent thereto.</li><li>Peddys Properties makes use of first-party and third-party cookies, as well as web beacons and similar technologies to deliver measurement services and targeted advertising to visitors. Cookies may include&nbsp;<a data-cke-saved-href=\"http://www.google.com/\" href=\"http://www.google.com/\">Google</a>&nbsp;Analytics cookies. No personal information is shared with any third party when providing these targeted services.</li><li>For detail regarding the information&nbsp;<a data-cke-saved-href=\"http://www.google.com/\" href=\"http://www.google.com/\">Google</a>&nbsp;collects and how it is used to deliver targeted advertising please visit <a data-cke-saved-href=\"http://www.google.com/policies/privacy/ads/\" href=\"http://www.google.com/policies/privacy/ads/\">http://www.google.com/policies/privacy/ads/</a></li><li>Peddys Propertiesis not responsible for and gives no warranties or makes any representations in respect of the privacy policies or practices of linked or any third party or advertised web sites.</li><li>Peddys Properties may at its discretion and from time to time disclose your personal information gathered through the site to selected partners for the purposes of marketing property, finance or insurance related services, consent for which is given by you utilising the site. Should you want Peddys Properties to cease disclosing your information you may send an e-mail to the following address and your details will be removed from the list:&nbsp;<a data-cke-saved-href=\"mailto:info@peddysproperties.com\" href=\"mailto:info@peddysproperties.com\">info@peddysproperties.com</a>. Personal information excludes, Physical address, Sold price and Date of property transfer.</li><li>Peddys Properties makes use of the Google Invisible ReCAPTCHA service. Use of the Google Invisible ReCAPTCHA service is subject to the&nbsp;<a data-cke-saved-href=\"https://www.google.com/intl/en/policies/privacy/\" href=\"https://www.google.com/intl/en/policies/privacy/\">Google Privacy Policy</a>&nbsp;and&nbsp;<a data-cke-saved-href=\"https://www.google.com/intl/en/policies/terms/\" href=\"https://www.google.com/intl/en/policies/terms/\">Terms of Use</a>.</li></ol><h3>Security</h3><ol><li>Users are prohibited from violating or attempting to violate the security of the Website, including, but without limitation:<ul><li>accessing data not intended for such user or logging into a server or account which the user is not authorized to access;</li><li>attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorization;</li><li>attempting to interfere with service to any user, host or network, including, without limitation, via means of submitting a virus to the website, overloading, \"flooding\", \"spamming\", \"mail bombing\" or \"crashing\";</li><li>sending unsolicited email, including promotions and/or advertising of products or services;</li><li>forging any TCP/IP packet header or any part of the header information in any email or newsgroup posting;</li><li>deleting or revising any material posted by any other person or entity;</li><li>using any device, software or routine to interfere or attempt to interfere with the proper working of this website or any activity being conducted on this site.</li></ul></li><li>Violations of system or network security may result in civil or criminal liability. Peddys Properties will investigate occurrences, which may involve such violations and may involve, and cooperate with, law enforcement authorities in prosecuting users who are involved in such violations.</li></ol><h3>Disclaimer</h3><ol><li>The website carries property advertisements, news, reviews and other content independently published by third parties on the website. We are involved in the buying, selling or development of the property.</li><li>Peddys Properties shall not be responsible for any user entering into agreements or making decision whatever nature in connection with the posting of property ads, property information, personal owned property information, use of financial calculators and/or the contents thereof and/or any other information obtained without the use of our Staff or Agents.</li><li>Whilst Peddys Properties has taken reasonable measures to ensure the integrity of the website and its contents, no warranty, whether express or implied, is given that the website will operate error-free or that any files, downloads or applications available via the website are free of viruses, trojans, bombs, time-locks or any other data, code or harmful mechanisms which has the ability to corrupt or affect the operation of your system.</li></ol><h3>Severability</h3><ol><li>These Terms &amp; Conditions constitute the entire agreement between Peddys Properties and you. Any failure by Peddys Properties to exercise or enforce any right or provision of these Terms &amp; Conditions shall in no way constitute a waiver of such right or provision.</li><li>In the event that any term or condition is not fully enforceable or valid for any reason, such term(s) or condition(s) shall be severable from the remaining terms and conditions. The remaining terms and conditions shall not be affected by such unenforceability or invalidity and shall remain enforceable and applicable.</li></ol><h3>Applicable Law</h3><p>This website is hosted, controlled and managed in the Republic of Cameroon, and thus, Cameroon law and jurisdiction govern the use or inability to use this website, or any other matter related to this website.</p><h3>Disputes</h3><p>All disputes in terms of this agreement or relating to the use or inability to use this web site shall be settled by arbitration conducted in English in terms of the rules of the Cameroon law. Such arbitration shall be held in Limbe, and the unsuccessful party shall pay all costs incurred by the successful party in attending and preparing for such arbitration.</p><h3>Peddys Properties E-mail Disclaimer</h3><ol><li>This message and any accompanying attachment(s) may contain confidential and copyrighted information. If you are not the addressee(s) indicated in this message or responsible for delivery of the message to the addressee(s), do not copy or deliver this message or the attachments to any other person including the intended recipient. Please destroy this message and notify Peddys Properties if this is the case.</li><li>Any comments and statements contained within this message should be seen as opinions only and not statements of fact. Peddys Properties will not accept liability for any loss suffered as a result of relying on this message unless the message indicates that the information is subject to an express warranty and has been authenticated by a digital signature capable of verifying the integrity of the message.</li><li>An e-mail is deemed to have been sent as soon as it is reflected in our service provider\'s mail server logs and is deemed to be received once we have either sent a delivery receipt or confirmed receipt by way of e-mail.</li><li>Peddys Properties takes all reasonable steps to ensure that this e-mail message is free from destructive code (such as viruses) but cannot guarantee the message is free from destructive code and so cannot be held liable for any loss or damage due to destructive code. Please inform us as soon as possible should you become aware of any destructive code that originated from this e-mail.</li><li>Peddys Properties may monitor e-mails in an attempt to ensure the quality of communications as well as for purposes of investigating or detecting the unauthorised use of the e-mail system. Please do not communicate via this e-mail system if you do not wish for your communications to be subjected to such scrutiny.</li></ol><p><br></p><p><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2020_02_24_073339_create_medias_table', 1),
(7, '2020_02_24_073655_create_categories_table', 1),
(8, '2020_02_24_094503_create_menu_table', 1),
(9, '2020_04_13_142641_create_options_table', 1),
(10, '2020_04_13_144019_create_terms_table', 1),
(11, '2020_04_13_144038_create_meta_table', 1),
(12, '2020_04_13_144625_create_post_category_table', 1),
(13, '2020_04_16_110534_create_customizers_table', 1),
(14, '2020_06_13_060141_create_user_meta_table', 1),
(15, '2020_08_24_062655_create_categorymetas_table', 1),
(16, '2020_10_24_101523_create_sessions_table', 1),
(17, '2020_10_24_110433_create_permission_tables', 1),
(18, '2020_10_29_145642_create_mediaposts_table', 1),
(19, '2020_10_29_152641_create_categoryusers_table', 1),
(20, '2020_10_31_074524_create_categoryrelations_table', 1),
(21, '2020_11_03_145224_create_postcategoryoptions_table', 1),
(22, '2020_11_14_083753_create_termrelations_table', 1),
(23, '2020_12_05_135234_create_prices_table', 1),
(24, '2020_12_07_084540_create_reviews_table', 1),
(25, '2020_12_07_171004_create_transactions_table', 1),
(26, '2021_01_03_133141_create_terms_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'theme_data', '{\"theme_color\":\"#9cc25d\",\"socials\":[{\"icon\":\"fa fa-pinterest-p\",\"url\":\"#\"},{\"icon\":\"fa fa-pinterest-p\",\"url\":\"#\"},{\"icon\":\"fa fa-instagram\",\"url\":\"#\"},{\"icon\":\"fa fa-twitter\",\"url\":\"#\"}],\"back_to_top\":\"enable\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(2, 'seo', '{\"title\":\"Property for sale | Houses for sale | Land for sale | Cameroon |\",\"description\":null,\"canonical\":null,\"tags\":null,\"twitterTitle\":null}', '2021-06-01 10:33:36', '2021-06-16 11:32:56'),
(3, 'langlist', '{\"English\":\"en\",\"Malay\":\"bn\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(4, 'default_lat_long', '{\"latitude\":\"23.8103\",\"longitude\":\"90.4125\",\"zoom\":\"10\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(5, 'lp_filesystem', '{\"compress\":5,\"system_type\":\"local\",\"system_url\":null}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(6, 'payment_settings', '{\"currency_name\":\"USD\",\"currency_icon\":\"$\",\"currency_position\":\"left\"}', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(7, 'breadcrumb', 'http://localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/01/11012116103582515ffc1deba3f0f.webp', '2021-01-21 07:01:43', '2021-01-21 18:35:59'),
(8, 'theme_color', '#414cdc', '2021-01-21 18:30:07', '2021-01-21 18:51:15'),
(9, 'listing_page', 'without_map', '2021-01-23 05:46:37', '2021-01-23 05:51:44');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 'dashboard', 'web', 'dashboard', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(2, 'admin.create', 'web', 'admin', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(3, 'admin.edit', 'web', 'admin', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(4, 'admin.update', 'web', 'admin', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(5, 'admin.delete', 'web', 'admin', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(6, 'admin.list', 'web', 'admin', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(7, 'role.create', 'web', 'role', '2021-06-01 10:33:29', '2021-06-01 10:33:29'),
(8, 'role.edit', 'web', 'role', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(9, 'role.update', 'web', 'role', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(10, 'role.delete', 'web', 'role', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(11, 'role.list', 'web', 'role', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(12, 'media.list', 'web', 'media', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(13, 'media.upload', 'web', 'media', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(14, 'media.delete', 'web', 'media', '2021-06-01 10:33:30', '2021-06-01 10:33:30'),
(15, 'page.create', 'web', 'Pages', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(16, 'page.edit', 'web', 'Pages', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(17, 'page.delete', 'web', 'Pages', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(18, 'page.list', 'web', 'Pages', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(19, 'blog.create', 'web', 'Blogs', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(20, 'blog.edit', 'web', 'Blogs', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(21, 'blog.delete', 'web', 'Blogs', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(22, 'blog.list', 'web', 'Blogs', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(23, 'system.settings', 'web', 'Developer', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(24, 'theme.option', 'web', 'Appearance', '2021-06-01 10:33:31', '2021-06-01 10:33:31'),
(25, 'theme', 'web', 'Appearance', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(26, 'menu', 'web', 'Appearance', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(27, 'seo', 'web', 'settings', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(28, 'filesystem', 'web', 'settings', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(29, 'backup', 'web', 'settings', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(30, 'language_edit', 'web', 'language', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(31, 'states.list', 'web', 'Location', '2021-06-01 10:33:32', '2021-06-01 10:33:32'),
(32, 'states.create', 'web', 'Location', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(33, 'states.edit', 'web', 'Location', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(34, 'cities.list', 'web', 'Location', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(35, 'cities.create', 'web', 'Location', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(36, 'cities.edit', 'web', 'Location', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(37, 'testimonial.list', 'web', 'Testimonials', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(38, 'testimonial.create', 'web', 'Testimonials', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(39, 'testimonial.edit', 'web', 'Testimonials', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(40, 'testimonial.delete', 'web', 'Testimonials', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(41, 'package.list', 'web', 'Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(42, 'package.create', 'web', 'Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(43, 'package.edit', 'web', 'Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(44, 'package.delete', 'web', 'Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(45, 'agency_package.list', 'web', 'Agency Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(46, 'agency_package.create', 'web', 'Agency Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(47, 'agency_package.edit', 'web', 'Agency Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(48, 'agency_package.delete', 'web', 'Agency Package', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(49, 'user.list', 'web', 'Agent & User', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(50, 'user.create', 'web', 'Agent & User', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(51, 'user.edit', 'web', 'Agent & User', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(52, 'user.delete', 'web', 'Agent & User', '2021-06-01 10:33:33', '2021-06-01 10:33:33'),
(53, 'agency.list', 'web', 'Agency', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(54, 'agency.create', 'web', 'Agency', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(55, 'agency.edit', 'web', 'Agency', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(56, 'agency.delete', 'web', 'Agency', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(57, 'api_and_getway', 'web', 'Payment Method', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(58, 'transactions', 'web', 'Payment Method', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(59, 'payment.settings', 'web', 'Payment Method', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(60, 'review.list', 'web', 'Review & Rattings', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(61, 'review.delete', 'web', 'Review & Rattings', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(62, 'Properties.list', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(63, 'Properties.create', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(64, 'Properties.edit', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(65, 'Properties.delete', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(66, 'project.list', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(67, 'project.create', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(68, 'project.edit', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(69, 'project.delete', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(70, 'feature.list', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(71, 'feature.edit', 'web', 'Real state', '2021-06-01 10:33:34', '2021-06-01 10:33:34'),
(72, 'feature.create', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(73, 'facilities.list', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(74, 'facilities.create', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(75, 'facilities.edit', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(76, 'category.list', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(77, 'category.create', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(78, 'category.edit', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(79, 'investor.list', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(80, 'investor.create', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(81, 'investor.edit', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(82, 'currency.list', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(83, 'currency.create', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(84, 'currency.edit', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(85, 'status.create', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(86, 'status.edit', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(87, 'status.delete', 'web', 'Real state', '2021-06-01 10:33:35', '2021-06-01 10:33:35'),
(88, 'status.list', 'web', 'Real state', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(89, 'input.list', 'web', 'Real state', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(90, 'input.create', 'web', 'Real state', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(91, 'input.edit', 'web', 'Real state', '2021-06-01 10:33:36', '2021-06-01 10:33:36'),
(92, 'input.delete', 'web', 'Real state', '2021-06-01 10:33:36', '2021-06-01 10:33:36');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `postcategoryoptions`
--

CREATE TABLE `postcategoryoptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `postcategoryoptions`
--

INSERT INTO `postcategoryoptions` (`id`, `category_id`, `term_id`, `type`, `value`) VALUES
(1, 21, 5, 'city', 'Dhaka, Bangladesh'),
(2, 21, 5, 'latitude', '23.810332'),
(3, 21, 5, 'longitude', '90.4125181'),
(31, 24, 6, 'city', 'Cox\'s Bazar, Bangladesh'),
(32, 24, 6, 'latitude', '21.4272283'),
(33, 24, 6, 'longitude', '92.0058074'),
(58, 22, 7, 'city', 'Faridpur, Bangladesh'),
(59, 22, 7, 'latitude', '23.6018691'),
(60, 22, 7, 'longitude', '89.8333382'),
(99, 17, 7, 'options', '12'),
(100, 16, 7, 'options', '32'),
(101, 41, 7, 'facilities', '10'),
(102, 42, 7, 'facilities', '12'),
(103, 43, 7, 'facilities', '5'),
(104, 44, 7, 'facilities', '13'),
(105, 45, 7, 'facilities', '12'),
(106, 46, 7, 'facilities', '10'),
(107, 47, 7, 'facilities', '8'),
(108, 48, 7, 'facilities', '23'),
(109, 49, 7, 'facilities', '10'),
(110, 50, 7, 'facilities', '5'),
(111, 51, 7, 'facilities', '23'),
(124, 17, 6, 'options', '12'),
(125, 16, 6, 'options', '10'),
(126, 41, 6, 'facilities', '12'),
(127, 42, 6, 'facilities', '31'),
(128, 43, 6, 'facilities', '23'),
(129, 44, 6, 'facilities', '34'),
(130, 45, 6, 'facilities', '12'),
(131, 46, 6, 'facilities', '10'),
(132, 47, 6, 'facilities', '8'),
(133, 48, 6, 'facilities', '10'),
(134, 49, 6, 'facilities', '9'),
(135, 50, 6, 'facilities', '10'),
(136, 51, 6, 'facilities', '23'),
(137, 17, 5, 'options', '2'),
(138, 16, 5, 'options', '4'),
(139, 41, 5, 'facilities', '12'),
(140, 42, 5, 'facilities', '14'),
(141, 43, 5, 'facilities', '23'),
(142, 44, 5, 'facilities', '13'),
(143, 45, 5, 'facilities', '16'),
(144, 45, 5, 'facilities', '18'),
(145, 46, 5, 'facilities', '10'),
(146, 47, 5, 'facilities', '12'),
(147, 48, 5, 'facilities', '8'),
(148, 49, 5, 'facilities', '15'),
(149, 50, 5, 'facilities', '10'),
(150, 51, 5, 'facilities', '13');

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE `post_category` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'category'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`term_id`, `category_id`, `type`) VALUES
(5, 5, 'category'),
(6, 1, 'category'),
(7, 2, 'category'),
(7, 35, 'features'),
(7, 36, 'features'),
(7, 37, 'features'),
(7, 38, 'features'),
(7, 39, 'features'),
(7, 40, 'features'),
(7, 29, 'status'),
(7, 19, 'state'),
(6, 35, 'features'),
(6, 36, 'features'),
(6, 37, 'features'),
(6, 38, 'features'),
(6, 39, 'features'),
(6, 40, 'features'),
(6, 27, 'status'),
(6, 20, 'state'),
(5, 35, 'features'),
(5, 36, 'features'),
(5, 37, 'features'),
(5, 38, 'features'),
(5, 39, 'features'),
(5, 40, 'features'),
(5, 26, 'status'),
(5, 19, 'state');

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `price` double DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'min_price'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `term_id`, `price`, `type`) VALUES
(1, 5, 55232, 'min_price'),
(2, 5, 99789, 'max_price'),
(3, 6, 79432, 'min_price'),
(4, 6, 123456, 'max_price'),
(5, 7, 55043, 'min_price'),
(6, 7, 82134, 'max_price');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_reported` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', '2021-06-01 10:33:28', '2021-06-01 10:33:28');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `termrelations`
--

CREATE TABLE `termrelations` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `child_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `count` double NOT NULL DEFAULT 0,
  `featured` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title`, `slug`, `user_id`, `status`, `type`, `count`, `featured`, `created_at`, `updated_at`) VALUES
(1, 'Basic', 'basic', 1, 1, 'package', 5, 23, '2021-01-10 12:35:03', '2021-01-10 12:35:03'),
(2, 'Premium', 'premium', 1, 1, 'package', 20, 50, '2021-01-10 12:35:28', '2021-01-10 12:35:28'),
(3, 'Enterprise', 'enterprise', 1, 1, 'package', 70, 100, '2021-01-10 12:36:08', '2021-01-10 12:36:08'),
(4, 'Enterprise Pro', 'enterprise-pro', 1, 1, 'package', 150, 150, '2021-01-10 12:37:09', '2021-01-10 12:37:09'),
(5, 'Dignissimos ut earum', 'dignissimos-ut-earum', 4, 1, 'property', 0, 0, '2021-01-10 12:42:02', '2021-01-10 12:49:16'),
(6, 'Quibusdam sed quos a', 'quibusdam-sed-quos-a', 3, 1, 'property', 0, 0, '2021-01-11 04:32:15', '2021-01-11 04:38:35'),
(7, 'Laudantium sint au', 'laudantium-sint-au', 2, 1, 'property', 0, 0, '2021-01-11 04:41:48', '2021-01-11 04:47:18'),
(8, 'Directory Will Be A Thing Of The Past.', 'macy-baker', 1, 1, 'blog', 0, 0, '2021-01-11 05:54:21', '2021-01-11 05:55:31'),
(9, 'The Most Inspiring Interior Design Of 2021', 'the-most-inspiring-interior-design-of-2021', 1, 1, 'blog', 0, 0, '2021-01-11 06:00:21', '2021-01-11 06:00:21'),
(10, '7 Instagram accounts for interior design enthusiasts', '7-instagram-accounts-for-interior-design-enthusiasts', 1, 1, 'blog', 0, 0, '2021-01-11 06:01:25', '2021-01-11 06:01:25'),
(11, 'Terms And Condition', 'terms-and-condition', 1, 1, 'page', 0, 0, '2021-06-06 13:24:36', '2021-06-06 13:27:43');

-- --------------------------------------------------------

--
-- Table structure for table `terms_user`
--

CREATE TABLE `terms_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `terms_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms_user`
--

INSERT INTO `terms_user` (`id`, `terms_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 5, 4, '2021-01-10 13:26:55', '2021-01-10 13:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `credits` int(11) NOT NULL DEFAULT 0,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credits` double NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `avatar`, `credits`, `status`, `name`, `slug`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'https://ui-avatars.com/api/?size=50&background=random&name=Admin', 0, 1, 'Admin', 'admin', 'admin@admin.com', NULL, '$2y$10$pVTnnNTSiSD/RDvbowt9i.JqehHrpeAAKAxUDx4CJ8hjB9tfKmXci', NULL, NULL, 'rfV92UqlywCkqIxsQkJ7QdjcD7l9swyiV2zMXOAIeLEmUuvlE81km4IkOlK4', '2021-06-01 10:33:28', '2021-06-01 10:33:28'),
(2, 2, '//localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/06/160621162386159960ca295f31433.webp', 0, 1, 'Godden Zama', 'godden-zama', 'info@peddysproperties.com', NULL, '$2y$10$JJTnkLXAbvjksd5ZnEO5N.8FEYKxQg81rP7v5k/QWWr/3jEzcvMBG', NULL, NULL, NULL, '2021-01-10 11:49:27', '2021-06-16 11:40:13'),
(3, 2, '//localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/06/160621162386152960ca291940b4c.webp', 0, 1, 'Tangang Minette', 'tangang-minette', 'minette@peddysproperties.com', NULL, '$2y$10$rw9l8G6oKZRFGzBt0FwBN..P1JYNTda0ElNy0c7PfYXxvhN55nZc.', NULL, NULL, NULL, '2021-01-10 12:20:24', '2021-06-16 11:38:59'),
(4, 2, '//localhost/codecanyon-QFiDLrJw-jomidar-laravel-real-estate-agency-portal/files/uploads/21/06/160621162386144460ca28c41f4c2.webp', 0, 1, 'Ewanga Bridgette', 'ewanga-bridgette', 'bridgette@peddysproperties.com', NULL, '$2y$10$08PwkxcrxxyNk6gl5VtruewBHiBOmaPtJw8cUNrQOBABmU9Ercsuq', NULL, NULL, NULL, '2021-01-10 12:23:44', '2021-06-16 11:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'preview',
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_meta`
--

INSERT INTO `user_meta` (`id`, `user_id`, `type`, `content`, `created_at`, `updated_at`) VALUES
(1, 2, 'credit', '30', '2021-01-10 11:49:27', '2021-01-10 12:18:05'),
(2, 2, 'content', '{\"address\":null,\"phone\":null,\"description\":\"CEO\",\"facebook\":\"#\",\"twitter\":\"#\",\"youtube\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/in\\/godden-zama-ndenge-2a879b213\\/\",\"instagram\":\"#\",\"whatsapp\":null,\"service_area\":null,\"tax_number\":null,\"license\":null}', '2021-01-10 11:49:27', '2021-06-16 11:40:13'),
(3, 3, 'credit', '0', '2021-01-10 12:20:24', '2021-01-10 12:21:20'),
(4, 3, 'content', '{\"address\":null,\"phone\":null,\"description\":\"Sales Agent\",\"facebook\":\"#\",\"twitter\":\"#\",\"youtube\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/in\\/tangang-minette-6415bb210\\/\",\"instagram\":\"#\",\"whatsapp\":null,\"service_area\":null,\"tax_number\":null,\"license\":null}', '2021-01-10 12:20:24', '2021-06-16 11:38:59'),
(5, 4, 'credit', NULL, '2021-01-10 12:23:44', '2021-01-10 12:23:44'),
(6, 4, 'content', '{\"address\":null,\"phone\":null,\"description\":\"Sales Agent\",\"facebook\":\"#\",\"twitter\":\"#\",\"youtube\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/in\\/ewanga-etara-99a4761b2\",\"instagram\":\"#\",\"whatsapp\":null,\"service_area\":null,\"tax_number\":null,\"license\":null}', '2021-01-10 12:23:44', '2021-06-16 11:36:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_user_id_foreign` (`user_id`);

--
-- Indexes for table `categorymetas`
--
ALTER TABLE `categorymetas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorymetas_category_id_foreign` (`category_id`);

--
-- Indexes for table `categoryrelations`
--
ALTER TABLE `categoryrelations`
  ADD KEY `categoryrelations_parent_id_foreign` (`parent_id`),
  ADD KEY `categoryrelations_child_id_foreign` (`child_id`);

--
-- Indexes for table `categoryusers`
--
ALTER TABLE `categoryusers`
  ADD KEY `categoryusers_user_id_foreign` (`user_id`),
  ADD KEY `categoryusers_category_id_foreign` (`category_id`);

--
-- Indexes for table `customizers`
--
ALTER TABLE `customizers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `mediaposts`
--
ALTER TABLE `mediaposts`
  ADD KEY `mediaposts_term_id_foreign` (`term_id`),
  ADD KEY `mediaposts_media_id_foreign` (`media_id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medias_user_id_foreign` (`user_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_term_id_foreign` (`term_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `postcategoryoptions`
--
ALTER TABLE `postcategoryoptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postcategoryoptions_category_id_foreign` (`category_id`),
  ADD KEY `postcategoryoptions_term_id_foreign` (`term_id`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD KEY `post_category_term_id_foreign` (`term_id`),
  ADD KEY `post_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prices_term_id_foreign` (`term_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_term_id_foreign` (`term_id`),
  ADD KEY `reviews_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `termrelations`
--
ALTER TABLE `termrelations`
  ADD KEY `termrelations_term_id_foreign` (`term_id`),
  ADD KEY `termrelations_child_id_foreign` (`child_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `terms_user_id_foreign` (`user_id`);

--
-- Indexes for table `terms_user`
--
ALTER TABLE `terms_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `terms_user_terms_id_foreign` (`terms_id`),
  ADD KEY `terms_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`),
  ADD KEY `transactions_term_id_foreign` (`term_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_meta_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `categorymetas`
--
ALTER TABLE `categorymetas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `customizers`
--
ALTER TABLE `customizers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `meta`
--
ALTER TABLE `meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `postcategoryoptions`
--
ALTER TABLE `postcategoryoptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `terms_user`
--
ALTER TABLE `terms_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categorymetas`
--
ALTER TABLE `categorymetas`
  ADD CONSTRAINT `categorymetas_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categoryrelations`
--
ALTER TABLE `categoryrelations`
  ADD CONSTRAINT `categoryrelations_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categoryrelations_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categoryusers`
--
ALTER TABLE `categoryusers`
  ADD CONSTRAINT `categoryusers_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categoryusers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mediaposts`
--
ALTER TABLE `mediaposts`
  ADD CONSTRAINT `mediaposts_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mediaposts_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `medias`
--
ALTER TABLE `medias`
  ADD CONSTRAINT `medias_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `meta`
--
ALTER TABLE `meta`
  ADD CONSTRAINT `meta_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `postcategoryoptions`
--
ALTER TABLE `postcategoryoptions`
  ADD CONSTRAINT `postcategoryoptions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `postcategoryoptions_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_category`
--
ALTER TABLE `post_category`
  ADD CONSTRAINT `post_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_category_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `prices_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `termrelations`
--
ALTER TABLE `termrelations`
  ADD CONSTRAINT `termrelations_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `termrelations_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `terms`
--
ALTER TABLE `terms`
  ADD CONSTRAINT `terms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `terms_user`
--
ALTER TABLE `terms_user`
  ADD CONSTRAINT `terms_user_terms_id_foreign` FOREIGN KEY (`terms_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `terms_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD CONSTRAINT `user_meta_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
