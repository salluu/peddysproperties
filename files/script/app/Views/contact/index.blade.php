@extends('theme::layouts.app')

@section('content')
    <style>
        #contactform select {
            height: 60px;
            padding: 0 20px;
            border: 1px solid #ddd;
        }
    </style>
    <section>
        <div class="hero-area hero-demo-2 breadcrumb" style="background-image: url('{{ breadcrumb() }}');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-area-start text-center">
                            <div class="breadcrumb-content">
                                <h2>{{ __('Contact Us') }}</h2>
                                <div class="breadcrumb-menu">
                                    <nav>
                                        <ul>
                                            <li><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                                            <li><span class="iconify" data-icon="ri:arrow-right-s-line"
                                                      data-inline="false"></span></li>
                                            <li>{{ __('Contact Us') }}</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="contact-area mt-100 mb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('contact.store') }}" method="POST" id="contactform">
                        @csrf
                        <div class="contact-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="contact-main-title text-center">
                                        <h3>{{ __('Let\'s Get Started') }}</h3>
                                        <p>{{ __('We will love to hear from you.') }}</p>
                                    </div>
                                </div>
                            </div>

                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ __('First Name') }}</label>
                                        <input type="text" placeholder="{{ __('First Name') }}" class="form-control"
                                               name="first_name">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ __('Last Name') }}</label>
                                        <input type="text" placeholder="{{ __('Last Name') }}" class="form-control"
                                               name="last_name">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ __('Email') }}</label>
                                        <input type="email" placeholder="{{ __('Email') }}" class="form-control"
                                               name="email">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ __('Phone Number') }}</label>
                                        <input type="text" placeholder="{{ __('Phone Number') }}" class="form-control"
                                               name="phone_number">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ __('Preferred contact method') }}</label>
                                        <select class="custom-select form-control"
                                                name="contact_method">
                                            <option value="phone">{{ __('Phone') }}</option>
                                            <option value="email">{{ __('Email') }}</option>
                                            <option value="text">{{ __('Text') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{ __('What are you looking for') }}?</label>
                                        <select class="custom-select form-control"
                                                name="looking_for">
                                            <option value="looking_to_buy">{{ __('Looking to Buy') }}</option>
                                            <option value="looking_to_rent">{{ __('Looking to Rent') }}</option>
                                            <option value="looking_to_sell">{{ __('Looking to Sell') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{ __('Message') }}</label>
                                        <textarea name="message" cols="30" rows="8" placeholder="{{ __('Message') }}"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
{{--                                @if(env('NOCAPTCHA_SITEKEY') != null)--}}
                                    <div class="col-lg-12 mb-3">
                                        {!! NoCaptcha::renderJs() !!}
                                        {!! NoCaptcha::display() !!}
                                    </div>
{{--                                @endif--}}
                                <div class="col-lg-12">
                                    <button type="submit" class="basicbtn">{{ __('Send Message') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/js/sweetalert2.all.min.js') }}"></script>
@endpush
