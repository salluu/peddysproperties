<?php

namespace Amcoders\Theme\jomidar\http\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Amcoders\Plugin\contactform\Contact;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Options;
use Illuminate\Support\Facades\Validator;

class ContactController extends controller
{
    public function index()
    {
        $seo = Options::where('key', 'seo')->first();
        $seo = json_decode($seo->value);

        SEOMeta::setTitle('Contact us');
        SEOMeta::setDescription($seo->description);
        SEOMeta::setCanonical($seo->canonical);

        OpenGraph::setDescription($seo->description);
        OpenGraph::setTitle('Contact us');
        OpenGraph::setUrl($seo->canonical);
        OpenGraph::addProperty('keywords', $seo->tags);

        TwitterCard::setTitle('Contact us');
        TwitterCard::setSite($seo->twitterTitle);

        JsonLd::setTitle('Contact us');
        JsonLd::setDescription($seo->description);
        JsonLd::addImage(asset(content('header', 'logo')));


        SEOTools::setTitle('Contact us');
        SEOTools::setDescription($seo->description);
        SEOTools::opengraph()->setUrl(url('/'));
        SEOTools::setCanonical($seo->canonical);
        SEOTools::opengraph()->addProperty('keywords', $seo->tags);
        SEOTools::twitter()->setSite($seo->twitterTitle);
        SEOTools::jsonLd()->addImage(asset(content('header', 'logo')));

        $key = '6LetJj0bAAAAADyWF_-gfrdVvLftbeybiuX5trxR';
        return view('view::contact.index', compact('key'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'looking_for' => 'required',
            'message' => 'required',
            'contact_method' => 'required',
        ]);

        if (env('NOCAPTCHA_SITEKEY') != null) {
            $messages = [
                'g-recaptcha-response.required' => 'You must check the reCAPTCHA.',
                'g-recaptcha-response.captcha' => 'Captcha error! try again later or contact site admin.',
            ];

            $validator = Validator::make($request->all(), [
                'g-recaptcha-response' => 'required|captcha'
            ], $messages);
        }

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()[0]]);
        }

        Contact::send($request->first_name, $request->last_name, $request->email, $request->phone_number, $request->looking_for, $request->message, $request->contact_method);

        return response()->json('success');
    }
}
