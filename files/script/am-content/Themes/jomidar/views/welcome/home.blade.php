@extends('theme::layouts.app')

@section('title','Jomidar')

@section('content')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
        .float {
            position: fixed;
            width: 60px;
            height: 60px;
            bottom: 40px;
            right: 40px;
            background-color: #25d366;
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            font-size: 30px;
            box-shadow: 2px 2px 3px #999;
            z-index: 100;
        }

        .my-float {
            margin-top: 16px;
        }
    </style>

    @include('view::layouts.section.hero.hero')

    @include('view::layouts.section.agents.agents')

    @include('view::layouts.section.properties.featured')

    <a href="https://api.whatsapp.com/send?phone=237675973373&text=Good%20day,%20I%20will%20like%20to%20inquire%20about%20a%20property%20listed%20on%20the%20Peddysproperties.com%20website"
       class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
@endsection

<input type="hidden" id="agent_url" value="{{ route('agent.data') }}">

@push('js')
    <script src="{{ asset('admin/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ theme_asset('jomidar/public/assets/js/home.js') }}"></script>
@endpush
