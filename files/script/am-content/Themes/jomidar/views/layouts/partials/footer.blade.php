<footer>
    <div class="footer-area footer-demo-1" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="footer-left-area">
                        <div class="footer-logo">
                            {{--                            <img id="footer_image" src="{{ asset(content('footer','footer_image')) }}" alt="">--}}
                            <img id="footer_image" src="{{ URL::to('uploads/logo_transparent.png') }}"
                                 style="width: 60px;" alt="logo">
                            <div class="footer-content">
                                <p id="footer_des">
                                    We offer the best real estate in Cameroon. Our trusted team of real estate agents,
                                    Architects, lawyers work together to ensure you have a smooth buying process. With
                                    Peddys Properties intuitive search interface, you can find and manage your favorite
                                    properties with ease
                                </p>
                            </div>
                            <div class="agent-social-links">
                                <nav>
                                    <ul>
                                        <li>
                                            <a target="_blank" href="https://web.facebook.com/peddysproperties">
                                                <span class="iconify" data-icon="ri:facebook-fill"
                                                      data-inline="false"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.linkedin.com/company/peddysproperties">
                                                <span class="iconify" data-icon="ri:linkedin-fill"
                                                      data-inline="false"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    {{--                    <div class="footer-menu">--}}
                    {{--                        {{ MenuCustom('Footer_left','','','','',true) }}--}}
                    {{--                    </div>--}}
                    <div class="footer-menu">
                        <div class="footer-menu-title">
                            <h4>Explore</h4>
                        </div>
                        <div class="footer-menu-content">
                            <nav>
                                <ul>
                                    <li class="">
                                        <a href="/page/about-us" target="_self">About Us</a>

                                    </li>
                                    <li class="">
                                        <a href="/list" target="_self">Property Listings</a>
                                    </li>
                                    <li class="">
{{--                                        <a href="/agent/favourites" target="_self">Bookmarks</a>--}}
                                        <a href="#" target="_self">Bookmarks</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    {{--                    <div class="footer-menu">--}}
                    {{--                        {{ MenuCustom('Footer_right','','','','',true) }}--}}
                    {{--                    </div>--}}
                    <div class="footer-menu">
                        <div class="footer-menu-title">
                            <h4>About</h4>
                        </div>
                        <div class="footer-menu-content">
                            <nav>
                                <ul>
                                    <li class="">
                                        <a href="/sitemap.xml" target="_self">Sitemap</a>
                                    </li>
                                    <li class="">
                                        <a href="/contact" target="_self">Contact Us</a>
                                    </li>
                                    <li class="">
                                        <a href="/page/terms-and-condition" target="_self">Terms And Condition</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area footer-demo-1">
        <div class="footer-bottom-content text-center">
            <span id="footer_copyright">
                Copyright © peddysproperties - {{ date('Y') }}
                {{--                {{ content('footer','footer_copyright') }}--}}
            </span>
        </div>
    </div>
</footer>
