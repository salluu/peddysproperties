<?php

namespace Amcoders\Plugin\contactform;

use SendGrid\Mail\Mail;
use SendGrid;
use Exception;

class Contact
{
    public static function send($first_name, $last_name, $mail, $phone_number, $looking_for, $message, $contact_method)
    {
        $data = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $mail,
            'phone_number' => $phone_number,
            'looking_for' => ucfirst(str_replace('_', ' ', $looking_for)),
            'contact_method' => $contact_method,
            'message' => $message
        ];

        $looking_for = ucfirst(str_replace('_', ' ', $looking_for));
        $email = new Mail();
        $email->setFrom("info@peddysproperties.com", "Peddysproperties");
        $email->setSubject("You have received a new query from " . $first_name);
        $email->addTo('info@peddysproperties.com', $first_name . " " . $last_name);
        $html = "<strong>Received a message from $first_name $last_name</strong><br /><strong>Phone Number $phone_number</strong><br /><strong>Looking for $looking_for</strong><br /><strong>Preferred contact method $contact_method</strong><br /><strong>$message</strong>";
        $email->addContent("text/html", $html);
        $sendgrid = new SendGrid('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');
        try {
            $sendgrid->send($email);
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }

        $looking_for = ucfirst(str_replace('_', ' ', $looking_for));
        $email = new Mail();
        $email->setFrom("info@peddysproperties.com", "Peddysproperties");
        $email->setSubject("We have received your query.");
        $email->addTo($mail, $first_name);
        $html = "<strong>Hi! We have received your query and we are happy to assist you.</strong><br /><strong>Our Support team will contact you soon.</strong>";
        $email->addContent("text/html", $html);
        $sendgrid = new SendGrid('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');
        try {
            $sendgrid->send($email);
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }

//        Mail::to(env("MAIL_TO"))->send(new ContactMail($data));
        return response()->json(['Mail Sent Success']);
    }
}
